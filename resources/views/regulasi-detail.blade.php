@extends('layouts.app')
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class=" container ">
        <!--begin::Notice-->
        <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
            <div class="alert-icon">
                <span class="svg-icon svg-icon-primary svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"/>
                            <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg><!--end::Svg Icon-->
                </span>	
            </div>
            <div class="alert-text">
                <h3 class="">
                    {{ $row->Peraturan ?? 'Peraturan' }} - {{ $row->Regulasi ?? 'Regulasi' }}
                    <input type="hidden" name="RegulasiID" id="RegulasiID" value="{{ $row->ID ?? '' }}">
                </h3>
            </div>
        </div>
        <!--end::Notice-->
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">
            
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <!--begin::Page Title-->
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item"><a href="/regulasi" class="text-muted">Regulasi</a></li>
                    <li class="breadcrumb-item"><a class="text-dark">{{ $row->Peraturan ?? 'Peraturan' }} - {{ $row->Regulasi ?? 'Regulasi' }}</a></li>
                </ul>
            </div>
            <!--end::Page Heading-->
        </div>
        <!--end::Info-->
        <!--begin::Card-->
        <div class="card card-custom mt-5">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">
                        Ringkasan
                    </h3>
                </div>
            </div>
            <hr>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 mb-5">
                        <h4>Tentang</h4>
                        <br>
                        <div>
                            Kelompok: {{ $row->Kelompok ?? '' }}
                        </div>
                        <div>
                            Bidang: {{ $row->Bidang ?? '' }}
                        </div>
                        <div class="mt-5">
                            @if (isset($row->File))
                            <a class="btn btn-outline-primary" href="/regulasiFile/{{ $row->File }}" target="_blank">Baca PDF Selengkapnya</a>
                            @else
                            <a class="btn btn-outline-primary" href="/regulasiFile/{{ $row->File }}" target="_blank" style="pointer-events: none;cursor: default;">Baca PDF Selengkapnya</a>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-9 mt-5">
                        <h4>Cakupan Regulasi</h4>
                        <br>
                        <div>
                            {{ $row->CakupanRegulasi ?? '' }}
                        </div>
                    </div>
                </div>
                
                <div class="mt-10">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#StatusBasin">
                                <span class="nav-icon"><i class="flaticon2-file-2"></i></span>
                                <span class="nav-text">Status Basin</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#StatusIsland">
                                <span class="nav-icon"><i class="flaticon2-file-2"></i></span>
                                <span class="nav-text">Status Island</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="StatusBasin" role="tabpanel" aria-labelledby="StatusBasin">
                        <div class="row mt-10">
                            <div class="col-lg-12 mb-10">
                                <div class="d-flex justify-content-between mb-5">
                                    <div>
                                        <h4>Compliance</h4>
                                    </div>
                                    <div>
                                        <h4>{{ floor($complied_percentage_island) ?? '' }}% regulasi telah comply</h4>
                                    </div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{ floor($percentage_island) ?? 0 }}%" aria-valuenow="{{ $complied_island ?? 0 }}" aria-valuemin="0" aria-valuemax="{{ $all_complience ?? 0 }}"></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="">Total Compliance</label>
                                    <h1>{{ number_format($all_complience) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-primary">Complied</label>
                                    <h1>{{ number_format($complied_island) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-danger">Not Complied</label>
                                    <h1>{{ number_format($not_complied_island) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-secondary">Not Applicable</label>
                                    <h1>{{ number_format($not_applicable_island) ?? 0 }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="StatusIsland" role="tabpanel" aria-labelledby="StatusIsland">
                        <div class="row mt-10">
                            <div class="col-lg-12 mb-10">
                                <div class="d-flex justify-content-between mb-5">
                                    <div>
                                        <h4>Compliance</h4>
                                    </div>
                                    <div>
                                        <h4>{{ floor($complied_percentage_basin) ?? '' }}% regulasi telah comply</h4>
                                    </div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{ floor($percentage_basin) ?? 0 }}%" aria-valuenow="{{ $complied_basin ?? 0 }}" aria-valuemin="0" aria-valuemax="{{ $all_complience ?? 0 }}"></div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="">Total Complience</label>
                                    <h1>{{ number_format($all_complience) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-primary">Complied</label>
                                    <h1>{{ number_format($complied_basin) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-danger">Not Complied</label>
                                    <h1>{{ number_format($not_complied_basin) ?? 0 }}</h1>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6 mb-5 text-center">
                                <div style="border: 1px dotted #cecece" class="p-5 rounded">
                                    <label for="" class="text-secondary">Not Applicable</label>
                                    <h1>{{ number_format($not_applicable_basin) ?? 0 }}</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Card-->
        </div>
        <!--end::Card-->
        <div class="card card-custom mt-5">
            <div class="card-header card-header-tabs-line">
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-bold nav-tabs-line">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1_4">
                                <span class="nav-icon"><i class="flaticon2-chat-1"></i></span>
                                <span class="nav-text">Compliance</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2_4">
                                <span class="nav-icon"><i class="flaticon2-file-2"></i></span>
                                <span class="nav-text">Tindakan Lanjut</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body" style="padding: 0;">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="kt_tab_pane_1_4" role="tabpanel" aria-labelledby="kt_tab_pane_1_4">
                        @include('complience-tabs')
                    </div>
                    <div class="tab-pane fade" id="kt_tab_pane_2_4" role="tabpanel" aria-labelledby="kt_tab_pane_2_4">
                        @include('tindak-lanjut-tabs')
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card card-custom mt-5">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">
                        Konsekuensi
                    </h3>
                </div>
            </div>
            <hr>
            <div class="card-body">
                <div>
                    {{ $row->Konsekuensi ?? '' }}
                </div>
                <br>
                <br>
                <!--begin::Notice-->
                <div class="alert alert-custom alert-light-primary alert-shadow gutter-b" role="alert">
                    <div class="alert-icon">
                        <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Home\Library.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000"/>
                                <rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>
                    </span>	
                </div>
                <div class="alert-text text-dark">
                    <h5>Apa itu Konsekuensi?</h5>
                    <p>
                        Konsekuensi dalam perundang-undangan mengacu pada akibat atau dampak yang muncul ketika seseorang atau suatu badan melakukan pelanggaran atau tidak mematuhi ketentuan yang telah ditetapkan dalam undang-undang atau peraturan. Konsekuensi dapat berupa sanksi atau hukuman yang diberikan oleh pihak yang berwenang untuk melindungi hak-hak dan kepentingan masyarakat atau individu.
                    </p>
                    <p>
                        Contoh konsekuensi dalam perundang-undangan antara lain denda, penjara, pemutusan hubungan kerja, pencabutan lisensi, penghentian usaha, dan sebagainya. Konsekuensi ini diharapkan dapat mencegah terjadinya pelanggaran di masa yang akan datang dan memastikan kepatuhan terhadap peraturan yang telah ditetapkan.
                    </p>
                </div>
            </div>
            <!--end::Notice-->
        </div>
    </div>
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@section('js')
<script src="/assets/js/pages/crud/forms/widgets/select2.js"></script>
<script>
    $('.kt_select2_1').select2({
        allowClear: true
    });
</script>

<script>
    $('.kt_select2_2').select2({
        allowClear: false
    });
</script>
<script type="text/javascript">
    $(function(){
        var maxlength = 500;
        $(".TextLimit").keyup(function(){
            var currentlength = $(this).val().length;
            var remained = maxlength-currentlength;
            $(".written").html(currentlength+" / ");
            $(".remained").html(maxlength+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_1 = 1400;
        $(".TextLimit_1").keyup(function(){
            var currentlength_1 = $(this).val().length;
            var remained_1 = maxlength_1-currentlength_1;
            $(".written_1").html(currentlength_1+" / ");
            $(".remained_1").html(maxlength_1+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_2 = 200;
        $(".TextLimit_2").keyup(function(){
            var currentlength_2 = $(this).val().length;
            var remained_2 = maxlength_2-currentlength_2;
            $(".written_2").html(currentlength_2+" / ");
            $(".remained_2").html(maxlength_2+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_3 = 1000;
        $(".TextLimit_3").keyup(function(){
            var currentlength_3 = $(this).val().length;
            var remained_3 = maxlength_3-currentlength_3;
            $(".written_3").html(currentlength_3+" / ");
            $(".remained_3").html(maxlength_3+" Karakter tersisa");
        });
    });
</script>

<script>
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".drop-zone");
        
        dropZoneElement.addEventListener("click", (e) => {
            inputElement.click();
        });
        
        inputElement.addEventListener("change", (e) => {
            if (inputElement.files.length) {
                updateThumbnail(dropZoneElement, inputElement.files[0]);
            }
        });
        
        dropZoneElement.addEventListener("dragover", (e) => {
            e.preventDefault();
            dropZoneElement.classList.add("drop-zone--over");
        });
        
        ["dragleave", "dragend"].forEach((type) => {
            dropZoneElement.addEventListener(type, (e) => {
                dropZoneElement.classList.remove("drop-zone--over");
            });
        });
        
        dropZoneElement.addEventListener("drop", (e) => {
            e.preventDefault();
            
            if (e.dataTransfer.files.length) {
                inputElement.files = e.dataTransfer.files;
                updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
            }
            
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });
    
    /**
    * Updates the thumbnail on a drop zone element.
    *
    * @param {HTMLElement} dropZoneElement
    * @param {File} file
    */
    function updateThumbnail(dropZoneElement, file) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
        
        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }
        
        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
            thumbnailElement = document.createElement("div");
            thumbnailElement.classList.add("drop-zone__thumb");
            dropZoneElement.appendChild(thumbnailElement);
        }
        
        thumbnailElement.dataset.label = file.name;
        
        // Show thumbnail for image files
        if (file.type.startsWith("image/")) {
            const reader = new FileReader();
            
            reader.readAsDataURL(file);
            reader.onload = () => {
                thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
            };
        } else {
            thumbnailElement.style.backgroundImage = null;
        }
    }
    
</script>

<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
{{-- <script src="/assets/complience.js"></script> --}}
<script src="/assets/tindakLanjut.js?{{ date('YmdHis') }}"></script>
<script type="text/javascript">
    $('.Pasal_Choose').change(function(){
        var array = $(this).val();
        if(array){
            $.ajax({
                type:"GET",
                url:"/complience/list/ayat/" + array,
                dataType: 'JSON',
                success:function(res){
                    if(res){
                        $(".Ayat_Select").empty();
                        $.each(res,function(name,id){
                            $(".Ayat_Select").append('<option value="'+name+'">'+name+'</option>');
                            $('.Ayat').val($('.AyatVal').val()).trigger('change');
                        });
                    } 
                }
            });
        }else{
            $(".Ayat_Select").empty();
        }
    });
</script>
<script>
    $('.summernote').summernote({
        height: 200,
        tabsize: 2
    });
</script>
@endsection