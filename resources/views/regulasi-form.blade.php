@extends('layouts.app')
<style>
    .drop-zone {
        max-width: 200px;
        height: 200px;
        padding: 25px;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
        font-family: "Quicksand", sans-serif;
        font-weight: 500;
        font-size: 20px;
        cursor: pointer;
        color: #cccccc;
        border: 2px dashed #00A3FF;
        border-radius: 10px;
    }
    
    .drop-zone--over {
        border-style: solid;
    }
    
    .drop-zone__input {
        display: none;
    }
    
    .drop-zone__thumb {
        width: 100%;
        height: 100%;
        border-radius: 10px;
        overflow: hidden;
        background-color: #cccccc;
        background-size: cover;
        position: relative;
    }
    
    .drop-zone__thumb::after {
        content: attr(data-label);
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        padding: 5px 0;
        color: #ffffff;
        background: rgba(0, 0, 0, 0.75);
        font-size: 14px;
        text-align: center;
    }
</style>
@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class=" container ">
        <!--begin::Notice-->
        <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
            <div class="alert-icon">
                <span class="svg-icon svg-icon-primary svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"/>
                            <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"/>
                        </g>
                    </svg><!--end::Svg Icon-->
                </span>	
            </div>
            <div class="alert-text">
                @if (isset($row))
                <h3 class="">
                    Perbaharui Regulasi
                </h3>
                @else
                <h3 class="">
                    Form Regulasi
                </h3>
                @endif
            </div>
        </div>
        <!--end::Notice-->
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">
            
            <!--begin::Page Heading-->
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <!--begin::Page Title-->
                <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item"><a href="/regulasi" class="text-muted">Regulasi</a></li>
                    @if (isset($row))
                    <li class="breadcrumb-item"><a class="text-dark">Perbaharui Regulasi</a></li>
                    @else
                    <li class="breadcrumb-item"><a class="text-dark">Form Regulasi</a></li>
                    @endif
                </ul>
            </div>
            <!--end::Page Heading-->
        </div>
        <!--end::Info-->
        <!--begin::Card-->
        <form class="form" action="{{ route('regulasi-save') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="ID" value="{{ isset($row) ? $row->ID : "" }}">
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Regulasi
                        </h3>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Peraturan <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="Peraturan" id="" class="form-control kt_select2_1 Peraturan" style="width:100%!important;" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>
                                <option value="" selected disabled>Pilih Peraturan</option>
                                <option value="UU" {{ isset($row) ? ($row->Peraturan == 'UU' ? 'selected' : '') : '' }}>UU</option>
                                <option value="PERMEN" {{ isset($row) ? ($row->Peraturan == 'PERMEN' ? 'selected' : '') : '' }}>PERMEN</option>
                                <option value="PP" {{ isset($row) ? ($row->Peraturan == 'PP' ? 'selected' : '') : '' }}>PP</option>
                                <option value="KEPRES" {{ isset($row) ? ($row->Peraturan == 'KEPRES' ? 'selected' : '') : '' }}>KEPRES</option>
                                <option value="INPRES" {{ isset($row) ? ($row->Peraturan == 'INPRES' ? 'selected' : '') : '' }}>INPRES</option>
                                <option value="KEPMEN" {{ isset($row) ? ($row->Peraturan == 'KEPMEN' ? 'selected' : '') : '' }}>KEPMEN</option>
                                <option value="KEPDITJEN" {{ isset($row) ? ($row->Peraturan == 'KEPDITJEN' ? 'selected' : '') : '' }}>KEPDITJEN</option>
                                <option value="SE" {{ isset($row) ? ($row->Peraturan == 'SE' ? 'selected' : '') : '' }}>SE</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">No <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control No" name="No" value="{{ isset($row) ? $row->No : "" }}" type="number" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Tahun <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control Tahun" min="1500" name="Tahun" value="{{ isset($row) ? $row->Tahun : "" }}" type="number" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Kelompok <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="Kelompok" id="" class="form-control kt_select2_1 Kelompok" style="width:100%!important;" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>
                                <option value="" selected disabled>Pilih Kelompok</option>
                                <option value="General HSE" {{ isset($row) ? ($row->Kelompok == 'General HSE' ? 'selected' : '') : '' }}>General HSE</option>
                                <option value="Hygiene and Sanitation" {{ isset($row) ? ($row->Kelompok == 'Hygiene and Sanitation' ? 'selected' : '') : '' }}>Hygiene and Sanitation</option>
                                <option value="Equipment Safety" {{ isset($row) ? ($row->Kelompok == 'Equipment Safety' ? 'selected' : '') : '' }}>Equipment Safety</option>
                                <option value="Personal Safety" {{ isset($row) ? ($row->Kelompok == 'Personal Safety' ? 'selected' : '') : '' }}>Personal Safety</option>
                                <option value="Hazardous Substance" {{ isset($row) ? ($row->Kelompok == 'Hazardous Substance' ? 'selected' : '') : '' }}>Hazardous Substance</option>
                                <option value="Competency" {{ isset($row) ? ($row->Kelompok == 'Competency' ? 'selected' : '') : '' }}>Competency</option>
                                <option value="Fire Protection" {{ isset($row) ? ($row->Kelompok == 'Fire Protection' ? 'selected' : '') : '' }}>Fire Protection</option>
                                <option value="Medical" {{ isset($row) ? ($row->Kelompok == 'Medical' ? 'selected' : '') : '' }}>Medical</option>
                                <option value="Permit" {{ isset($row) ? ($row->Kelompok == 'Permit' ? 'selected' : '') : '' }}>Permit</option>
                                <option value="Pollution" {{ isset($row) ? ($row->Kelompok == 'Pollution' ? 'selected' : '') : '' }}>Pollution</option>
                                <option value="Radiation" {{ isset($row) ? ($row->Kelompok == 'Radiation' ? 'selected' : '') : '' }}>Radiation</option>
                                <option value="Chemical Safety" {{ isset($row) ? ($row->Kelompok == 'Chemical Safety' ? 'selected' : '') : '' }}>Chemical Safety</option>
                                <option value="Emission" {{ isset($row) ? ($row->Kelompok == 'Emission' ? 'selected' : '') : '' }}>Emission</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Bidang <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="Bidang" id="" class="form-control kt_select2_1 Bidang" style="width:100%!important;" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>
                                <option value="" selected disabled>Pilih Bidang</option>
                                <option value="HSE" {{ isset($row) ? ($row->Bidang == 'HSE' ? 'selected' : '') : '' }}>HSE</option>
                                <option value="H" {{ isset($row) ? ($row->Bidang == 'H' ? 'selected' : '') : '' }}>H</option>
                                <option value="S" {{ isset($row) ? ($row->Bidang == 'S' ? 'selected' : '') : '' }}>S</option>
                                <option value="M" {{ isset($row) ? ($row->Bidang == 'M' ? 'selected' : '') : '' }}>M</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Topik <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control Topik" name="Topik" value="{{ isset($row) ? $row->Topik : "" }}" type="text" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Regulasi <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control Regulasi" name="Regulasi" value="{{ isset($row) ? $row->Regulasi : "" }}" type="text" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Unggah PDF</label>
                        <div class="col-9">
                            <div class="drop-zone">
                                <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                <input type="file" name="File" class="drop-zone__input">
                            </div>
                            <input type="hidden" name="oldFile" value="{{ isset($row) ? $row->File : "" }}">
                            @if (isset($row))
                            <br><small><b>File: {{ $row->File }}</b></small>
                            @endif
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Card-->
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Cakupan Regulasi
                        </h3>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-12">
                            <textarea class="form-control CakupanRegulasi TextLimit" name="CakupanRegulasi" rows="6" maxlength="500" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>{{ isset($row) ? $row->CakupanRegulasi : "" }}</textarea>
                            <br>
                            <small><span class="written"></span><span class="remained"></span></small>
                        </div>
                    </div>
                </div>
            </div>
            @if (!isset($row))
            <div id="kt_repeater_1" class="kt_repeater_1">
                <div class="mt-8 mb-5 d-flex justify-content-between">
                    <h3>Daftar Compliance</h3>
                    <div class="">
                        <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                </div>
                <div class="card card-custom" data-repeater-list="kt_docs_repeater_basic">
                    <div data-repeater-item>
                        <div class="card-header flex-wrap border-0 pt-6 pb-0 d-flex justify-content-between">
                            <div class="">
                                <h3 class="card-label">
                                    Compliance
                                </h3>
                            </div>
                            <div class="" style="visibility: visible;">
                                <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                    <i class="la la-trash-o"></i>Delete
                                </a>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="form-group mb-3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Pasal <span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input class="form-control Pasal" name="Pasal" value="{{ isset($row) ? $row->Pasal : "" }}" type="number" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Ayat <span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input class="form-control Ayat" name="Ayat" value="{{ isset($row) ? $row->Ayat : "" }}" type="number" id="" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Judul <span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <textarea class="form-control Complience TextLimit_2" name="Complience" rows="3" maxlength="200" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required></textarea>
                                                <br>
                                                <small><span class="written_2"></span><span class="remained_2"></span></small>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Status Basin <span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <select name="StatusBasin" id="" class="form-control StatusBasin" style="width:100%!important;" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>
                                                    <option value="Complied">Complied</option>
                                                    <option value="Not Complied">Not Complied</option>
                                                    <option value="Not Applicable">Not Applicable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Status Island <span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <select name="StatusIsland" id="" class="form-control StatusIsland" style="width:100%!important;" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>
                                                    <option value="Complied">Complied</option>
                                                    <option value="Not Complied">Not Complied</option>
                                                    <option value="Not Applicable">Not Applicable</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-3 col-form-label">Key Items</label>
                                            <div class="col-9">
                                                <textarea class="form-control KeyItems TextLimit_1 summernote" name="KeyItems" rows="6" maxlength="1400"></textarea>
                                                <br>
                                                <small><span class="written_1"></span><span class="remained_1"></span></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="card card-custom mt-5">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">
                            Konsekuensi
                        </h3>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-12">
                            <textarea class="form-control Konsekuensi TextLimit_3" name="Konsekuensi" rows="6" maxlength="1000" oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')" required>{{ isset($row) ? $row->Konsekuensi : "" }}</textarea>
                            <br>
                            <small><span class="written_3"></span><span class="remained_3"></span></small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-9"></div>
                <div class="col-lg-3 text-right">
                    <a href="/regulasi" class="btn btn-secondary">Batal</a>
                    @if (isset($row))
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    @else
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    @endif
                </div>
            </div>
        </form>
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
@endsection

@section('js')
{{-- <script src="/assets/js/pages/crud/forms/widgets/form-repeater.js"></script> --}}
{{-- <script src="/assets/js/pages/crud/forms/editors/summernote.js"></script> --}}
<script>
    $('.kt_repeater_1').repeater({
        initEmpty: false,
        
        defaultValues: {
            'text-input': 'foo'
        },
        
        show: function () {
            $(this).slideDown();
            $(this).find('.summernote').summernote({
                height: 200,
                tabsize: 2
            });
        },
        
        hide: function (deleteElement) {                
            $(this).slideUp(deleteElement);                 
        },
        
        ready: function(){
            $('.summernote').summernote({
                height: 200,
                tabsize: 2
            });
        } 
    });
    
    $('.summernote').summernote({
        height: 200,
        tabsize: 2
    });
</script>
<script src="/assets/js/pages/crud/forms/widgets/select2.js"></script>
<script>
    $('.kt_select2_1').select2({
        allowClear: true
    });
</script>
<script type="text/javascript">
    $(function(){
        var maxlength = 500;
        $(".TextLimit").keyup(function(){
            var currentlength = $(this).val().length;
            var remained = maxlength-currentlength;
            $(".written").html(currentlength+" / ");
            $(".remained").html(maxlength+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_1 = 1400;
        $(".TextLimit_1").keyup(function(){
            var currentlength_1 = $(this).val().length;
            var remained_1 = maxlength_1-currentlength_1;
            $(".written_1").html(currentlength_1+" / ");
            $(".remained_1").html(maxlength_1+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_2 = 200;
        $(".TextLimit_2").keyup(function(){
            var currentlength_2 = $(this).val().length;
            var remained_2 = maxlength_2-currentlength_2;
            $(".written_2").html(currentlength_2+" / ");
            $(".remained_2").html(maxlength_2+" Karakter tersisa");
        });
    });
    $(function(){
        var maxlength_3 = 1000;
        $(".TextLimit_3").keyup(function(){
            var currentlength_3 = $(this).val().length;
            var remained_3 = maxlength_3-currentlength_3;
            $(".written_3").html(currentlength_3+" / ");
            $(".remained_3").html(maxlength_3+" Karakter tersisa");
        });
    });
</script>

<script>
    document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
        const dropZoneElement = inputElement.closest(".drop-zone");
        
        dropZoneElement.addEventListener("click", (e) => {
            inputElement.click();
        });
        
        inputElement.addEventListener("change", (e) => {
            if (inputElement.files.length) {
                updateThumbnail(dropZoneElement, inputElement.files[0]);
            }
        });
        
        dropZoneElement.addEventListener("dragover", (e) => {
            e.preventDefault();
            dropZoneElement.classList.add("drop-zone--over");
        });
        
        ["dragleave", "dragend"].forEach((type) => {
            dropZoneElement.addEventListener(type, (e) => {
                dropZoneElement.classList.remove("drop-zone--over");
            });
        });
        
        dropZoneElement.addEventListener("drop", (e) => {
            e.preventDefault();
            
            if (e.dataTransfer.files.length) {
                inputElement.files = e.dataTransfer.files;
                updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
            }
            
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });
    
    /**
    * Updates the thumbnail on a drop zone element.
    *
    * @param {HTMLElement} dropZoneElement
    * @param {File} file
    */
    function updateThumbnail(dropZoneElement, file) {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
        
        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }
        
        // First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
            thumbnailElement = document.createElement("div");
            thumbnailElement.classList.add("drop-zone__thumb");
            dropZoneElement.appendChild(thumbnailElement);
        }
        
        thumbnailElement.dataset.label = file.name;
        
        // Show thumbnail for image files
        if (file.type.startsWith("image/")) {
            const reader = new FileReader();
            
            reader.readAsDataURL(file);
            reader.onload = () => {
                thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
            };
        } else {
            thumbnailElement.style.backgroundImage = null;
        }
    }
    
</script>
@endsection