@extends('layouts.app')

@section('content')
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class=" container ">
        <!--begin::Card-->
        <div class="row">
            <div class="col-md-4">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-header card-header-tabs-line">
                        {{-- <div class="card-title">
                            <h3 class="card-label">Card Line Tabs</h3>
                        </div> --}}
                        <div class="card-toolbar">
                            <ul class="nav nav-tabs nav-bold nav-tabs-line">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tab_pane_1_2">Status Basin</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tab_pane_2_2">Status Island</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="kt_tab_pane_1_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                <div class="box-white h-100">
                                    <h1 class="fw-bold text-dark mb-3">{{ floor($complied_percentage_basin) ?? 0 }}%</h1>
                                    <div class="tamu mt-4">
                                        <h4 class="fs-16">Regulasi telah comply</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="ComplienceBasin" style="height: 150px; width: 100%"></div>
                                        </div>
                                        <div class="col-md-6 align-self-center">
                                            <ul class="list-unstyled fw-semibold">
                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Code\Minus.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <rect fill="#6EBC44" x="6" y="11" width="12" height="2" rx="1"/>
                                                            </g>
                                                        </svg><!--end::Svg Icon--></span><b class="text-muted">Complied</b>
                                                    </div>
                                                    <div>
                                                        <b>{{ floor($complied_percentage_basin) ?? 0 }}%</b> 
                                                    </div>
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <span class="svg-icon svg-icon-danger svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Code\Minus.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <rect fill="#F65A49" x="6" y="11" width="12" height="2" rx="1"/>
                                                            </g>
                                                        </svg><!--end::Svg Icon--></span><b class="text-muted">Not Complied</b>
                                                    </div>
                                                    <div>
                                                        <b>{{ floor($not_complied_percentage_basin) ?? 0 }}%</b>
                                                    </div>
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center">
                                                    <div>
                                                        <span class="svg-icon svg-icon-secondary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Code\Minus.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <rect fill="#6d6e73" x="6" y="11" width="12" height="2" rx="1"/>
                                                            </g>
                                                        </svg><!--end::Svg Icon--></span><b class="text-muted">Not Applicable</b>
                                                    </div>
                                                    <div>
                                                        <b>{{ floor($not_applicable_percentage_basin) ?? 0 }}%</b>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="kt_tab_pane_2_2" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                                <div class="box-white h-100">
                                    <h1 class="fw-bold text-dark mb-3">{{ floor($complied_percentage_island) ?? 0 }}%</h1>
                                    <div class="tamu mt-4">
                                        <h4 class="fs-16">Regulasi telah comply</h4>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div id="ComplienceIsland" style="height: 150px; width: 100%"></div>
                                        </div>
                                        <div class="col-md-6 align-self-center">
                                            <ul class="list-unstyled fw-semibold">
                                                <li class="d-flex justify-content-between align-items-center mb-3">
                                                    <span><i class="fa fa-minus" style="color: #6EBC44;"></i> <b class="text-muted">Complied</b></span> <b>{{ floor($complied_percentage_island) ?? 0 }}%</b> 
                                                </li>
                                                <li class="d-flex justify-content-between align-items-center mb-3"><span><i class="fa fa-minus" style="color: #F65A49;"></i> <b class="text-muted">Not Complied</b></span> <b>{{ floor($not_complied_percentage_island) ?? 0 }}%</b> </li>
                                                <li class="d-flex justify-content-between align-items-center mb-3"><span><i class="fa fa-minus" style="color: #6d6e73;"></i> <b class="text-muted">Not Applicable</b></span> <b>{{ floor($not_applicable_percentage_island) ?? 0 }}%</b> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <div class="col-md-4">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body">
                        <div class="box-white h-100">
                            <div class="d-flex align-items-center flex-wrap mt-5 justify-content-between">
                                <div>
                                    <h1 class="fw-bold text-dark mb-3">{{ $all_tindak_lanjut ?? 0 }}</h1>
                                    <div class="tamu mt-4">
                                        <h4 class="fs-16">Tindakan Lanjut</h4>
                                    </div>
                                </div>
                                <div>
                                    <a href="/tindak-lanjut" class="btn btn-primary">Lihat Semua</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="">
                        @if ($all_tindak_lanjut != 0)
                        <div id="tindakan_lanjut" class="card-rounded-bottom" data-color="danger" style="height: 150px; width: 100%"></div>
                        @else
                          <div class="d-flex justify-content-center align-items-center" style="margin-top: -30%">
                            <h5>Data Kosong</h5>  
                          </div>
                        @endif
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <div class="col-md-4">
                <!--begin::Card-->
                <div class="card card-custom gutter-b card-stretch">
                    <div class="card-body">
                        <div class="box-white h-100">
                            <div class="d-flex align-items-center flex-wrap mt-5 justify-content-between">
                                <div>
                                    <h1 class="fw-bold text-dark mb-3">{{ $all_regulasi ?? 0 }}</h1>
                                    <div class="tamu mt-4">
                                        <h4 class="fs-16">Regulasi</h4>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="">
                                <div class="d-flex align-items-center flex-wrap mt-5 justify-content-between">
                                    <h5 class="fs-14">Total Compliance</h5>
                                    <h5 class="fs-14">{{ $all_complience ?? 0 }}</h5>
                                </div>
                                <hr>
                                <div class="d-flex align-items-center flex-wrap mt-5 justify-content-between">
                                    <h5 class="fs-14">Total Kelompok</h5>
                                    <h5 class="fs-14">{{ $all_kelompok ?? 13 }}</h5>
                                </div>
                                <hr>
                                <div class="d-flex align-items-center flex-wrap mt-5 justify-content-between">
                                    <h5 class="fs-14">Total Bidang</h5>
                                    <h5 class="fs-14">{{ $all_bidang ?? 4 }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Card-->
            </div>
        </div>
        <!--end::Card-->
        <!--begin::Card-->
        <div class="card card-custom mt-5">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">
                        Regulasi
                    </h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a data-toggle="modal" data-target="#modalImport" class="btn btn-light text-primary font-weight-bolder mr-5 mb-2" style="background: white;">
                        <span class="svg-icon svg-icon-primary svg-icon-md"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Files\Import.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <rect fill="##6EBC44" opacity="0.3" transform="translate(12.000000, 7.000000) rotate(-180.000000) translate(-12.000000, -7.000000) " x="11" y="1" width="2" height="12" rx="1"/>
                                <path d="M17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L18,6 C20.209139,6 22,7.790861 22,10 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,9.99305689 C2,7.7839179 3.790861,5.99305689 6,5.99305689 L7.00000482,5.99305689 C7.55228957,5.99305689 8.00000482,6.44077214 8.00000482,6.99305689 C8.00000482,7.54534164 7.55228957,7.99305689 7.00000482,7.99305689 L6,7.99305689 C4.8954305,7.99305689 4,8.88848739 4,9.99305689 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,10 C20,8.8954305 19.1045695,8 18,8 L17,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <path d="M14.2928932,10.2928932 C14.6834175,9.90236893 15.3165825,9.90236893 15.7071068,10.2928932 C16.0976311,10.6834175 16.0976311,11.3165825 15.7071068,11.7071068 L12.7071068,14.7071068 C12.3165825,15.0976311 11.6834175,15.0976311 11.2928932,14.7071068 L8.29289322,11.7071068 C7.90236893,11.3165825 7.90236893,10.6834175 8.29289322,10.2928932 C8.68341751,9.90236893 9.31658249,9.90236893 9.70710678,10.2928932 L12,12.5857864 L14.2928932,10.2928932 Z" fill="##6EBC44" fill-rule="nonzero"/>
                            </g>
                        </svg><!--end::Svg Icon--></span>Import
                    </a>
                    <!--end::Button-->
                    <!--begin::Button-->
                    <a href="/regulasi/export" class="btn btn-light text-primary font-weight-bolder mr-5 mb-2" style="background: white;">
                        <span class="svg-icon svg-icon-primary svg-icon-md"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Files\Export.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L18,6 C20.209139,6 22,7.790861 22,10 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,9.99305689 C2,7.7839179 3.790861,5.99305689 6,5.99305689 L7.00000482,5.99305689 C7.55228957,5.99305689 8.00000482,6.44077214 8.00000482,6.99305689 C8.00000482,7.54534164 7.55228957,7.99305689 7.00000482,7.99305689 L6,7.99305689 C4.8954305,7.99305689 4,8.88848739 4,9.99305689 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,10 C20,8.8954305 19.1045695,8 18,8 L17,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) scale(1, -1) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="2" width="2" height="12" rx="1"/>
                                <path d="M12,2.58578644 L14.2928932,0.292893219 C14.6834175,-0.0976310729 15.3165825,-0.0976310729 15.7071068,0.292893219 C16.0976311,0.683417511 16.0976311,1.31658249 15.7071068,1.70710678 L12.7071068,4.70710678 C12.3165825,5.09763107 11.6834175,5.09763107 11.2928932,4.70710678 L8.29289322,1.70710678 C7.90236893,1.31658249 7.90236893,0.683417511 8.29289322,0.292893219 C8.68341751,-0.0976310729 9.31658249,-0.0976310729 9.70710678,0.292893219 L12,2.58578644 Z" fill="##6EBC44" fill-rule="nonzero" transform="translate(12.000000, 2.500000) scale(1, -1) translate(-12.000000, -2.500000) "/>
                            </g>
                        </svg><!--end::Svg Icon--></span>Export
                    </a>
                    <!--end::Button-->
                    
                    <!--begin::Button-->
                    <a href="/regulasi/form" class="btn btn-primary color-white font-weight-bolder mb-2">
                        <i class="la la-plus"></i> Tambah Regulasi
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            
            <div class="card-body">
                <!--begin: Search Form-->
                <div class="mt-2 mb-5 mt-lg-5 mb-lg-10">
                    <div class="row align-items-center">
                        <div class="col-lg-9 col-xl-8">
                            <div class="row align-items-center">
                                <div class="col-md-4 my-2 my-md-0">
                                    <div class="input-icon">
                                        <input type="text" class="form-control" placeholder="Regulasi" data-col-index="6"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-2 my-2 my-md-0">
                                    <div class="d-flex align-items-center">
                                        <select class="form-control kt_select2_1 datatable-input" data-col-index="2">
                                            <option value="" selected disabled>Tahun</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 my-2 my-md-0">
                                    <div class="d-flex align-items-center">
                                        <select class="form-control kt_select2_1" id="KelompokSearch">
                                            <option value="" selected disabled>Kelompok</option>
                                            <option value="General HSE">General HSE</option>
                                            <option value="Hygiene and Sanitation">Hygiene and Sanitation</option>
                                            <option value="Equipment Safety">Equipment Safety</option>
                                            <option value="Personal Safety">Personal Safety</option>
                                            <option value="Hazardous Substance">Hazardous Substance</option>
                                            <option value="Competency">Competency</option>
                                            <option value="Fire Protection">Fire Protection</option>
                                            <option value="Medical">Medical</option>
                                            <option value="Permit">Permit</option>
                                            <option value="Pollution">Pollution</option>
                                            <option value="Radiation">Radiation</option>
                                            <option value="Chemical Safety">Chemical Safety</option>
                                            <option value="Emission">Emission</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-3 my-2 my-md-0">
                                    <div class="d-flex align-items-center">
                                        <select class="form-control kt_select2_1" id="BidangSearch">
                                            <option value="" selected disabled>Bidang</option>
                                            <option value="HSE">HSE</option>
                                            <option value="H">H</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0 row">
                            <div class="col-lg-12">
                                <button class="btn btn-primary btn-primary--icon" id="kt_search">
                                    <span>
                                        <i class="la la-search"></i>
                                        <span>Search</span>
                                    </span>
                                </button>
                                &nbsp;&nbsp;
                                <button class="btn btn-light btn-light--icon" id="kt_reset" style="background: white;">
                                    <span>
                                        <i class="la la-close"></i>
                                        <span>Reset</span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Search Form-->
                @if (session('error'))
                <div class="alert alert-danger"><i class="flaticon-exclamation text-white"></i> {{ session('error') }}</div>
                @elseif (session('success'))
                <div class="alert alert-success"><i class="flaticon-exclamation text-white"></i> {{ session('success') }}</div>
                @endif
                <!--begin: Datatable-->
                <table class="table table-bordered table-hover" id="kt_datatable">
                    <thead>
                        <tr>
                            <th>Peraturan</th>
                            <th>No</th>
                            <th>Tahun</th>
                            <th>Topik</th>
                            <th>Kelompok</th>
                            <th>Bidang</th>
                            <th>Regulasi</th>
                            <th>File PDF</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
<div class="modal fade modalImport" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="/regulasi/import" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Regulasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="ID" id="ID" class="ID">
                    <div class="form-group mb-3">
                        <a href="/assets/RegulasiImport.xlsx" class="text-primary" download><u>Download Template</u></a>
                        <hr>
                        <div class="">
                            <label>Upload File</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file" id="customFile"/>
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="/assets/js/pages/crud/forms/widgets/select2.js"></script>
<script>
    $('.kt_select2_1').select2({
        allowClear: false
    });
</script>
<script>
    $(function () {
        var data = [{
            "id": "idData",
            "name": "Data",
            "data":[
            {name: 'Complied', y: {{ floor($complied_percentage_basin) ?? 0 }}, color: '#6EBC44'},
            {name: 'Not Complied', y: {{ floor($not_complied_percentage_basin) ?? 0 }}, color: '#F65A49'},
            {name: 'Not Applicable', y: {{ floor($not_applicable_percentage_basin) ?? 0 }}, color: '#E4E6EF'},
            ]
        }];
        window.mychart = Highcharts.chart('ComplienceBasin', {
            chart: {
                type: 'pie',
                plotShadow: false,
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: '100%',
                    borderWidth: 20,
                    borderColor: null,
                    slicedOffset: 10,
                    dataLabels: {
                        enabled: false,
                    }
                },
            },
            title: {
                verticalAlign: 'middle',
                floating: false,
                text: ''
            },
            legend: {
                enabled: true,
                floating: true,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'middle',
                x: 40,
                y: 0,
            },
            series: data,
        });
        $('input[type="radio"]').on('click', function(event) {
            var value = $(this).val();
            window.mychart.series[0].setData([data[0].data[value]]);
            window.mychart.redraw();
        });
    });
</script>
<script>
    $(function () {
        var data = [{
            "id": "idData",
            "name": "Data",
            "data":[
            {name: 'Complied', y: {{ floor($complied_percentage_island) ?? 0 }}, color: '#6EBC44'},
            {name: 'Not Complied', y: {{ floor($not_complied_percentage_island) ?? 0 }}, color: '#F65A49'},
            {name: 'Not Applicable', y: {{ floor($not_applicable_percentage_island) ?? 0 }}, color: '#E4E6EF'},
            ]
        }];
        window.mychart = Highcharts.chart('ComplienceIsland', {
            chart: {
                type: 'pie',
                plotShadow: false,
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: '100%',
                    borderWidth: 20,
                    borderColor: null,
                    slicedOffset: 10,
                    dataLabels: {
                        enabled: false,
                    }
                },
            },
            title: {
                verticalAlign: 'middle',
                floating: false,
                text: ''
            },
            legend: {
                enabled: true,
                floating: true,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'middle',
                x: 40,
                y: 0,
            },
            series: data,
        });
        $('input[type="radio"]').on('click', function(event) {
            var value = $(this).val();
            window.mychart.series[0].setData([data[0].data[value]]);
            window.mychart.redraw();
        });
    });
    $(function(){
        var element = document.getElementById("tindakan_lanjut");
        var color = KTUtil.hasAttr(element, 'data-color') ? KTUtil.attr(element, 'data-color') : 'warning';
        var height = parseInt(KTUtil.css(element, 'height'));
        
        if (!element) {
            return;
        }
        
        var options = {
            series: [{
                name: 'Tindakan Lanjut',
                data: <?= $tindaklanjut ?>
            }],
            chart: {
                type: 'area',
                height: height,
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                sparkline: {
                    enabled: true
                }
            },
            plotOptions: {},
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'solid',
                opacity: 0.7
            },
            stroke: {
                curve: 'smooth',
                show: true,
                width: 1,
                colors: [KTApp.getSettings()['colors']['theme']['base'][color]]
            },
            xaxis: {
                categories: <?= $label ?>,
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                },
                crosshairs: {
                    show: false,
                    position: 'front',
                    stroke: {
                        color: KTApp.getSettings()['colors']['gray']['gray-300'],
                        width: 1,
                        dashArray: 3
                    }
                },
                tooltip: {
                    enabled: true,
                    formatter: undefined,
                    offsetY: 0,
                    style: {
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                // max: 60,
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
            },
            colors: [KTApp.getSettings()['colors']['theme']['light'][color]],
            markers: {
                colors: [KTApp.getSettings()['colors']['theme']['light'][color]],
                strokeColor: [KTApp.getSettings()['colors']['theme']['base'][color]],
                strokeWidth: 3
            }
        };
        
        var chart = new ApexCharts(element, options);
        chart.render();
    });
</script>
<script src="/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<script src="/assets/regulasi.js?{{ date('YmdHis') }}"></script>
@endsection