<!--begin::Card-->
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">
                Compliance
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a data-toggle="modal" data-target="#modalImport" class="btn btn-light text-primary font-weight-bolder mr-5 mb-2" style="background: white;">
                <span class="svg-icon svg-icon-primary svg-icon-md"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Files\Import.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <rect fill="##6EBC44" opacity="0.3" transform="translate(12.000000, 7.000000) rotate(-180.000000) translate(-12.000000, -7.000000) " x="11" y="1" width="2" height="12" rx="1"/>
                        <path d="M17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L18,6 C20.209139,6 22,7.790861 22,10 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,9.99305689 C2,7.7839179 3.790861,5.99305689 6,5.99305689 L7.00000482,5.99305689 C7.55228957,5.99305689 8.00000482,6.44077214 8.00000482,6.99305689 C8.00000482,7.54534164 7.55228957,7.99305689 7.00000482,7.99305689 L6,7.99305689 C4.8954305,7.99305689 4,8.88848739 4,9.99305689 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,10 C20,8.8954305 19.1045695,8 18,8 L17,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <path d="M14.2928932,10.2928932 C14.6834175,9.90236893 15.3165825,9.90236893 15.7071068,10.2928932 C16.0976311,10.6834175 16.0976311,11.3165825 15.7071068,11.7071068 L12.7071068,14.7071068 C12.3165825,15.0976311 11.6834175,15.0976311 11.2928932,14.7071068 L8.29289322,11.7071068 C7.90236893,11.3165825 7.90236893,10.6834175 8.29289322,10.2928932 C8.68341751,9.90236893 9.31658249,9.90236893 9.70710678,10.2928932 L12,12.5857864 L14.2928932,10.2928932 Z" fill="##6EBC44" fill-rule="nonzero"/>
                    </g>
                </svg><!--end::Svg Icon--></span>Import
            </a>
            <!--end::Button-->
            <!--begin::Button-->
            <a href="/complience/export/{{ $row->ID ?? '' }}" class="btn btn-light text-primary font-weight-bolder mr-5 mb-2" style="background: white;">
                <span class="svg-icon svg-icon-primary svg-icon-md"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo9\dist/../src/media/svg/icons\Files\Export.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L18,6 C20.209139,6 22,7.790861 22,10 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,9.99305689 C2,7.7839179 3.790861,5.99305689 6,5.99305689 L7.00000482,5.99305689 C7.55228957,5.99305689 8.00000482,6.44077214 8.00000482,6.99305689 C8.00000482,7.54534164 7.55228957,7.99305689 7.00000482,7.99305689 L6,7.99305689 C4.8954305,7.99305689 4,8.88848739 4,9.99305689 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,10 C20,8.8954305 19.1045695,8 18,8 L17,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) scale(1, -1) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="2" width="2" height="12" rx="1"/>
                        <path d="M12,2.58578644 L14.2928932,0.292893219 C14.6834175,-0.0976310729 15.3165825,-0.0976310729 15.7071068,0.292893219 C16.0976311,0.683417511 16.0976311,1.31658249 15.7071068,1.70710678 L12.7071068,4.70710678 C12.3165825,5.09763107 11.6834175,5.09763107 11.2928932,4.70710678 L8.29289322,1.70710678 C7.90236893,1.31658249 7.90236893,0.683417511 8.29289322,0.292893219 C8.68341751,-0.0976310729 9.31658249,-0.0976310729 9.70710678,0.292893219 L12,2.58578644 Z" fill="##6EBC44" fill-rule="nonzero" transform="translate(12.000000, 2.500000) scale(1, -1) translate(-12.000000, -2.500000) "/>
                    </g>
                </svg><!--end::Svg Icon--></span>Export
            </a>
            <!--end::Button-->
            
            <!--begin::Button-->
            <a data-toggle="modal" data-target="#modalAddComplience" class="btn btn-primary color-white font-weight-bolder mb-2">
                <i class="la la-plus"></i> Add Compliance
            </a>
            <!--end::Button-->
        </div>
    </div>
    
    <div class="card-body">
        <!--begin: Search Form-->
        <div class="mt-2 mb-5 mt-lg-5 mb-lg-10">
            <div class="row align-items-center">
                <div class="col-lg-12 col-xl-12">
                    <div class="row align-items-center">
                        <div class="col-md-3 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control datatable-input" placeholder="Compliance" data-col-index="2"/>
                                <span><i class="flaticon2-search-1 text-muted"></i></span>
                            </div>
                        </div>
                        
                        <div class="col-md-3 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <select class="form-control kt_select2_2" id="StatusSearchCompliedBasin">
                                    <option value="" selected disabled>Status Basin</option>
                                    <option value="Complied">Complied</option>
                                    <option value="Not Complied">Not Complied</option>
                                    <option value="Not Applicable">Not Applicable</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3 my-2 my-md-0 mb-3">
                            <div class="d-flex align-items-center">
                                <select class="form-control kt_select2_2" id="StatusSearchCompliedIsland">
                                    <option value="" selected disabled>Status Island</option>
                                    <option value="Complied">Complied</option>
                                    <option value="Not Complied">Not Complied</option>
                                    <option value="Not Applicable">Not Applicable</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <button class="btn btn-primary btn-primary--icon" id="kt_search_1">
                                <span>
                                    <i class="la la-search"></i>
                                    <span>Search</span>
                                </span>
                            </button>
                            &nbsp;&nbsp;
                            <button class="btn btn-light btn-light--icon" id="kt_reset" style="background: white;">
                                <span>
                                    <i class="la la-close"></i>
                                    <span>Reset</span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        @if (session('error'))
        <div class="alert alert-danger"><i class="flaticon-exclamation text-white"></i> {{ session('error') }}</div>
        @elseif (session('success'))
        <div class="alert alert-success"><i class="flaticon-exclamation text-white"></i> {{ session('success') }}</div>
        @endif
        <!--begin: Datatable-->
        <table class="table table-bordered table-hover" id="kt_datatable_complience">
            <thead>
                <tr>
                    <th>Pasal</th>
                    <th>Ayat</th>
                    <th>Compliance</th>
                    <th>Status Basin</th>
                    <th>Status Island</th>
                    <th>Key Items</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <!--end: Datatable-->
    </div>
</div>
<!--end::Card-->
<div class="modal fade modalUpdateComplience" id="modalUpdateComplience" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <form action="/complience/update" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Perbaharui Compliance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="ID" id="" class="ID">
                    <input type="hidden" name="RegulasiID" value="{{ $row->ID ?? '' }}">
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Pasal <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control Pasal" name="Pasal" type="number" id="" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Ayat <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control Ayat" name="Ayat" type="number" id="" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Judul <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <textarea class="form-control Complience TextLimit_2" name="Complience" rows="3" maxlength="200" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"></textarea>
                            <br>
                            <small><span class="written_2"></span><span class="remained_2"></span></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Status Basin <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="StatusBasin" id="" class="form-control kt_select2_1 StatusBasin" style="width:100%!important;" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')">
                                <option value="Complied">Complied</option>
                                <option value="Not Complied">Not Complied</option>
                                <option value="Not Applicable">Not Applicable</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Status Island <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="StatusIsland" id="" class="form-control kt_select2_1 StatusIsland" style="width:100%!important;" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')">
                                <option value="Complied">Complied</option>
                                <option value="Not Complied">Not Complied</option>
                                <option value="Not Applicable">Not Applicable</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Key Items</label>
                        <div class="col-9">
                            <textarea class="form-control KeyItems TextLimit_1 summernote" name="KeyItems" rows="6" maxlength="1400"></textarea>
                            <br>
                            <small><span class="written_1"></span><span class="remained_1"></span></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan Perubahan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modalAddComplience" id="modalAddComplience" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <form action="/complience/save" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Form Compliance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="RegulasiID" value="{{ $row->ID ?? '' }}">
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Pasal <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control" name="Pasal" type="number" id="" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Ayat <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input class="form-control" name="Ayat" type="number" id="" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Judul <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <textarea class="form-control TextLimit_2" name="Complience" rows="3" maxlength="200" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')"></textarea>
                            <br>
                            <small><span class="written_2"></span><span class="remained_2"></span></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Status Basin <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="StatusBasin" id="" class="form-control kt_select2_1" style="width:100%!important;" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')">
                                <option value="Complied">Complied</option>
                                <option value="Not Complied">Not Complied</option>
                                <option value="Not Applicable">Not Applicable</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Status Island <span class="text-danger">*</span></label>
                        <div class="col-9">
                            <select name="StatusIsland" id="" class="form-control kt_select2_1" style="width:100%!important;" required oninvalid="this.setCustomValidity('Mohon untuk mengisi kolom')" oninput="setCustomValidity('')">
                                <option value="Complied">Complied</option>
                                <option value="Not Complied">Not Complied</option>
                                <option value="Not Applicable">Not Applicable</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-3 col-form-label">Key Items</label>
                        <div class="col-9">
                            <textarea class="form-control TextLimit_1 summernote" name="KeyItems" rows="6" maxlength="1400"></textarea>
                            <br>
                            <small><span class="written_1"></span><span class="remained_1"></span></small>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modalKeyItems" id="modalKeyItems" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Compliance Key Items</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="KeyItems">
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary font-weight-bold">Selesai Baca</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade modalImport" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="/complience/import" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Compliance</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="RegulasiID" id="RegulasiID" value="{{ $row->ID ?? '' }}">
                    <div class="form-group mb-3">
                        <a href="/assets/ComplianceImport.xlsx" class="text-primary" download><u>Download Template</u></a>
                        <hr>
                        <div class="">
                            <label>Upload File</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file" id="customFile"/>
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>
