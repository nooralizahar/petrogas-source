-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 11, 2023 at 12:45 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petrogas`
--

-- --------------------------------------------------------

--
-- Table structure for table `complience`
--

DROP TABLE IF EXISTS `complience`;
CREATE TABLE IF NOT EXISTS `complience` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RegulasiID` int(11) DEFAULT NULL,
  `Pasal` int(11) DEFAULT NULL,
  `Ayat` int(11) DEFAULT NULL,
  `Complience` varchar(255) DEFAULT NULL,
  `StatusBasin` varchar(255) DEFAULT NULL,
  `StatusIsland` varchar(255) DEFAULT NULL,
  `KeyItems` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `regulasi`
--

DROP TABLE IF EXISTS `regulasi`;
CREATE TABLE IF NOT EXISTS `regulasi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Peraturan` varchar(255) DEFAULT NULL,
  `No` int(11) DEFAULT NULL,
  `Tahun` year(4) DEFAULT NULL,
  `Topik` varchar(255) DEFAULT NULL,
  `Kelompok` varchar(255) DEFAULT NULL,
  `Bidang` varchar(255) DEFAULT NULL,
  `Regulasi` varchar(255) DEFAULT NULL,
  `File` varchar(255) DEFAULT NULL,
  `CakupanRegulasi` text DEFAULT NULL,
  `Konsekuensi` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tindaklanjut`
--

DROP TABLE IF EXISTS `tindaklanjut`;
CREATE TABLE IF NOT EXISTS `tindaklanjut` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ComplienceID` int(11) DEFAULT NULL,
  `TindakLanjut` varchar(255) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `PIC` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
