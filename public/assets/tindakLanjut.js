"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = function() {
    
    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var initTable1 = function() {
        // begin first table
        var table = $('#kt_datatable_tindak').DataTable({
            responsive: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            
            lengthMenu: [5, 10, 25, 50],
            
            pageLength: 10,
            
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            
            searchDelay: 500,
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: {
                url: '/tindak-lanjut/ajax',
                type: 'POST',
                data: function(data) {
                    // Read values
                    var RegulasiID = $('#RegulasiID').val();
                    var StatusSearch = $('#StatusSearch').val();

                    // Append to data
                    data.searchByRegulasiID = RegulasiID;
                    data.searchByStatus = StatusSearch;
                    data._token = CSRF_TOKEN;
                    data.columnsDef = [
                        'Pasal', 'Ayat',
						'ComplienceID', 'TindakLanjut', 'DueDate',
						'Status', 'Action'
                    ];
                },
            },
            columns: [
				{data: 'Pasal'},
				{data: 'Ayat'},
				{data: 'ComplienceID', "width": "25%"},
				{data: 'TindakLanjut', "width": "25%", responsivePriority: -1},
				{data: 'DueDate'},
				{data: 'Status'},
				{data: 'Actions', orderable: false, responsivePriority: -1},
			],
            buttons: [{
                    extend: 'excel',    
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5]
                    }
                },
            ],
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    
                    switch (column.title()) {
                    }
                });
            },
            columnDefs: [
                {
					targets: 5,
					render: function(data, type, full, meta) {
						var status = {
							'Complete': {'title': 'Complete', 'state': 'primary'},
							'Not Complete': {'title': 'Not Complete', 'state': 'danger'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<button class="btn btn-light-' + status[data].state +' btn-sm font-weight-bold">' + status[data].title + '</button>';
					},
				},
                {
					targets: 6,
					width: '10%',
				},
            ],
        });

        $("#ExportReporttoExcel").on("click", function() {
            table.button('.buttons-excel').trigger();
        });

        $("#ExportReporttoCvs").on("click", function() {
            table.button('.buttons-csv').trigger();
        });
        
        var asdasd = function(value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table.column(index).search(val ? val : '', false, true);
        };
        
        $('#kt_search').on('click', function(e) {
            e.preventDefault();
            var params = {};
            $('.datatable-input').each(function() {
                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                table.column(i).search(val ? val : '', false, false);
            });
            table.table().draw();
        });
        
        $('#kt_reset').on('click', function(e) {
            e.preventDefault();
            $('.datatable-input').each(function() {
                $(this).val('');
                table.column($(this).data('col-index')).search('', false, false);
            });
            table.table().draw();
        });

        // begin first table
        var table_complience = $('#kt_datatable_complience').DataTable({
            responsive: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            
            lengthMenu: [5, 10, 25, 50],
            
            pageLength: 10,
            
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            
            searchDelay: 500,
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: {
                url: '/complience/ajax',
                type: 'POST',
                data: function(data) {
                    // Read values
                    var RegulasiID = $('#RegulasiID').val();
                    var StatusSearchCompliedBasin = $('#StatusSearchCompliedBasin').val();
                    var StatusSearchCompliedIsland = $('#StatusSearchCompliedIsland').val();

                    // Append to data
                    data.searchByRegulasiID = RegulasiID;
                    data.searchByStatusBasin = StatusSearchCompliedBasin;
                    data.searchByStatusIsland = StatusSearchCompliedIsland;
                    data._token = CSRF_TOKEN;
                    data.columnsDef = [
                        'Pasal', 'Ayat',
						'Complience', 'StatusBasin', 'StatusIsland', 'KeyItems', 'Action'
                    ];
                },
            },
            columns: [
				{data: 'Pasal'},
				{data: 'Ayat'},
				{data: 'Complience', responsivePriority: -1},
				{data: 'StatusBasin'},
				{data: 'StatusIsland'},
				{data: 'KeyItems', orderable: false},
				{data: 'Actions', orderable: false, responsivePriority: -1},
			],
            buttons: [{
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4]
                    }
                },
            ],
            initComplete: function() {
                this.api().columns().every(function() {
                    var column_complience = this;
                    
                    switch (column_complience.title()) {
                    }
                });
            },
            columnDefs: [
                {
					targets: 3,
					render: function(data, type, full, meta) {
						var status_complience_basin = {
							'Complied': {'title': 'Complied', 'state': 'primary'},
							'Not Complied': {'title': 'Not Complied', 'state': 'danger'},
							'Not Applicable': {'title': 'Not Applicable', 'state': 'dark'},
						};
						if (typeof status_complience_basin[data] === 'undefined') {
							return data;
						}
						return '<button class="btn btn-light-' + status_complience_basin[data].state +' btn-sm font-weight-bold">' + status_complience_basin[data].title + '</button>';
					},
				},
                {
					targets: 4,
					render: function(data, type, full, meta) {
						var status_complience_island = {
							'Complied': {'title': 'Complied', 'state': 'primary'},
							'Not Complied': {'title': 'Not Complied', 'state': 'danger'},
							'Not Applicable': {'title': 'Not Applicable', 'state': 'dark'},
						};
						if (typeof status_complience_island[data] === 'undefined') {
							return data;
						}
						return '<button class="btn btn-light-' + status_complience_island[data].state +' btn-sm font-weight-bold">' + status_complience_island[data].title + '</button>';
					},
				},
                {
					targets: 6,
					width: '10%',
				},
            ]
        });

        $("#ExportReporttoExcel").on("click", function() {
            table_complience.button('.buttons-excel').trigger();
        });

        $("#ExportReporttoCvs").on("click", function() {
            table_complience.button('.buttons-csv').trigger();
        });
        
        var asdasd = function(value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table_complience.column(index).search(val ? val : '', false, true);
        };
        
        $('#kt_search_1').on('click', function(e) {
            e.preventDefault();
            var params = {};
            $('.datatable-input').each(function() {
                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                table_complience.column(i).search(val ? val : '', false, false);
            });
            table_complience.table().draw();
        });
        
        $('#kt_reset_1').on('click', function(e) {
            e.preventDefault();
            $('.datatable-input').each(function() {
                $(this).val('');
                table_complience.column($(this).data('col-index')).search('', false, false);
            });
            table_complience.table().draw();
        });
        
    };
    
    return {
        
        //main function to initiate the module
        init: function() {
            initTable1();
        },
        
    };
    
}();

jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();
});