$(".delete").click(function(e) {
    e.preventDefault();
    const url = $(this).attr('href');
    Swal.fire({
        text: "Anda yakin ingin menghapus",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Ya, hapus",
        customClass: {
			confirmButton: "btn btn-danger",
			cancelButton: "btn btn-secondary"
		}
    }).then(function(result) {
        if (result.value) {
            window.location.href = url;
        }
    });
});