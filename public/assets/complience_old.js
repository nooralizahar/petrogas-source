"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = function() {
    
    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var initTable = function() {
        // begin first table
        var table_complience = $('#kt_datatable_complience').DataTable({
            responsive: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            
            lengthMenu: [5, 10, 25, 50],
            
            pageLength: 10,
            
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            
            searchDelay: 500,
            processing: true,
            serverSide: false,
            ajax: {
                url: '/complience/ajax',
                type: 'POST',
                data: function(data) {
                    // Read values
                    var regulasiID = $('#regulasiID').val();

                    // Append to data
                    data.regulasiID = regulasiID;
                    data._token = CSRF_TOKEN;
                    data.columnsDef = [
                        'Pasal', 'Ayat',
						'Complience', 'Status', 'KeyItems', 'Action'
                    ];
                },
            },
            columns: [
				{data: 'Pasal'},
				{data: 'Ayat'},
				{data: 'Complience'},
				{data: 'Status'},
				{data: 'KeyItems'},
				{data: 'Actions', responsivePriority: -1},
			],
            buttons: [{
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2]
                    }
                },
            ],
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    
                    switch (column.title()) {
                        case 'Status':
                        column.data().unique().sort().each(function(d, j) {
                            $('.datatable-input[data-col-index="5"]').append('<option value="' + d + '">' + d + '</option>');
                        });
                        break;
                    }
                });
            },
        });

        $("#ExportReporttoExcel").on("click", function() {
            table_complience.button('.buttons-excel').trigger();
        });

        $("#ExportReporttoCvs").on("click", function() {
            table_complience.button('.buttons-csv').trigger();
        });
        
        var asdasd = function(value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table_complience.column(index).search(val ? val : '', false, true);
        };
        
        $('#kt_search').on('click', function(e) {
            e.preventDefault();
            var params = {};
            $('.datatable-input').each(function() {
                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                table_complience.column(i).search(val ? val : '', false, false);
            });
            table_complience.table().draw();
        });
        
        $('#kt_reset').on('click', function(e) {
            e.preventDefault();
            $('.datatable-input').each(function() {
                $(this).val('');
                table_complience.column($(this).data('col-index')).search('', false, false);
            });
            table_complience.table().draw();
        });
        
    };
    
    return {
        
        //main function to initiate the module
        init: function() {
            initTable();
        },
        
    };
    
}();

jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();
});