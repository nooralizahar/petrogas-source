"use strict";
var KTDatatablesSearchOptionsAdvancedSearch = function() {
    
    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var initTable1 = function() {
        // begin first table
        var table = $('#kt_datatable').DataTable({
            responsive: true,
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            
            lengthMenu: [5, 10, 25, 50],
            
            pageLength: 10,
            
            language: {
                'lengthMenu': 'Display _MENU_',
            },
            
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: '/regulasi/ajax',
                type: 'POST',
                data: function(data) {
                    // Read values
                    var KelompokSearch = $('#KelompokSearch').val();
                    var BidangSearch = $('#BidangSearch').val();

                    // Append to data
                    data.searchByKelompok = KelompokSearch;
                    data.searchByBidang = BidangSearch;
                    data._token = CSRF_TOKEN;
                    data.columnsDef = [
                        'Peraturan', 'No',
                        'Tahun', 'Topik', 'Kelompok', 'Bidang',
                        'Regulasi', 'File', 'Actions'
                    ];
                },
            },
            columns: [
                {data: 'Peraturan'},
                {data: 'No'},
                {data: 'Tahun'},
                {data: 'Topik'},
                {data: 'Kelompok'},
                {data: 'Bidang'},
                {data: 'Regulasi'},
                {data: 'File', orderable: false},
                {data: 'Actions', orderable: false, responsivePriority: -1},
            ],
            buttons: [{
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6]
                    }
                },
            ],
            columnDefs: [
                {
					targets: 8,
					width: '12%',
				},
            ],
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    
                    switch (column.title()) {
                        case 'Tahun':
                        column.data().unique().sort().each(function(d, j) {
                            $('.datatable-input[data-col-index="2"]').append('<option value="' + d + '">' + d + '</option>');
                        });
                        break;
                    }
                });
            },
        });

        $("#ExportReporttoExcel").on("click", function() {
            table.button('.buttons-excel').trigger();
        });

        $("#ExportReporttoCvs").on("click", function() {
            table.button('.buttons-csv').trigger();
        });
        
        var asdasd = function(value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table.column(index).search(val ? val : '', false, true);
        };
        
        $('#kt_search').on('click', function(e) {
            e.preventDefault();
            var params = {};
            $('.datatable-input').each(function() {
                var i = $(this).data('col-index');
                if (params[i]) {
                    params[i] += '|' + $(this).val();
                }
                else {
                    params[i] = $(this).val();
                }
            });
            $.each(params, function(i, val) {
                // apply search params to datatable
                table.column(i).search(val ? val : '', false, false);
            });
            table.table().draw();
        });
        
        $('#kt_reset').on('click', function(e) {
            e.preventDefault();
            $('.datatable-input').each(function() {
                $(this).val('');
                table.column($(this).data('col-index')).search('', false, false);
            });
            table.table().draw();
        });
        
    };
    
    return {
        
        //main function to initiate the module
        init: function() {
            initTable1();
        },
        
    };
    
}();

jQuery(document).ready(function() {
    KTDatatablesSearchOptionsAdvancedSearch.init();
});