<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\RegulasiController::class, 'index']);
Route::get('home', [App\Http\Controllers\RegulasiController::class, 'index']);

Route::get('regulasi', [App\Http\Controllers\RegulasiController::class, 'index'])->name('regulasi');
Route::post('regulasi/ajax', [App\Http\Controllers\RegulasiController::class, 'ajax'])->name('regulasi-ajax');
Route::post('regulasi/save', [App\Http\Controllers\RegulasiController::class, 'save'])->name('regulasi-save');
Route::get('regulasi/form/{id?}', [App\Http\Controllers\RegulasiController::class, 'form'])->name('regulasi-form');
Route::get('regulasi/detail/{id?}', [App\Http\Controllers\RegulasiController::class, 'detail'])->name('regulasi-detail');
Route::get('regulasi/delete/{id?}', [App\Http\Controllers\RegulasiController::class, 'delete'])->name('regulasi-delete');
Route::get('regulasi/export/{id?}', [App\Http\Controllers\RegulasiController::class, 'export'])->name('regulasi-export');
Route::post('regulasi/import', [App\Http\Controllers\RegulasiController::class, 'import'])->name('regulasi-import');

Route::get('complience', [App\Http\Controllers\ComplienceController::class, 'index'])->name('complience');
Route::post('complience/ajax', [App\Http\Controllers\ComplienceController::class, 'ajax'])->name('complience-ajax');
Route::post('complience/save', [App\Http\Controllers\ComplienceController::class, 'save'])->name('complience-save');
Route::post('complience/update', [App\Http\Controllers\ComplienceController::class, 'update'])->name('complience-update');
Route::get('complience/delete/{id?}', [App\Http\Controllers\ComplienceController::class, 'delete'])->name('complience-delete');
Route::get('complience/export/{RegulasiID?}', [App\Http\Controllers\ComplienceController::class, 'export'])->name('complience-export');
Route::post('complience/import', [App\Http\Controllers\ComplienceController::class, 'import'])->name('complience-import');

Route::get('tindak-lanjut', [App\Http\Controllers\TindakLanjutController::class, 'index'])->name('tindak-lanjut');
Route::post('tindak-lanjut/ajax', [App\Http\Controllers\TindakLanjutController::class, 'ajax'])->name('tindak-lanjut-ajax');
Route::post('tindak-lanjut/save', [App\Http\Controllers\TindakLanjutController::class, 'save'])->name('tindak-lanjut-save');
Route::post('tindak-lanjut/update', [App\Http\Controllers\TindakLanjutController::class, 'update'])->name('tindak-lanjut-update');
Route::get('tindak-lanjut/delete/{id?}', [App\Http\Controllers\TindakLanjutController::class, 'delete'])->name('tindak-lanjut-delete');
Route::get('tindak-lanjut/export/{RegulasiID?}', [App\Http\Controllers\TindakLanjutController::class, 'export'])->name('tindak-lanjut-export');
Route::post('tindak-lanjut/import', [App\Http\Controllers\TindakLanjutController::class, 'import'])->name('tindak-lanjut-import');

Route::get('complience/list/ayat/{array?}', [App\Http\Controllers\TindakLanjutController::class, 'list_ayat'])->name('complience-list-ayat');