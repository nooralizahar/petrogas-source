<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regulasi extends Model
{
    use HasFactory;
    
    protected $table = 'regulasi';

    protected $fillable = [
        'Peraturan',
        'No',
        'Tahun',
        'Topik',
        'Kelompok',
        'Bidang',
        'Regulasi',
        'File',
        'CakupanRegulasi',
        'Konsekuensi',
    ];

    protected $primaryKey = 'ID';

    public function complienceChild(){
        return $this->hasMany(Complience::class, 'RegulasiID');
    }
}
