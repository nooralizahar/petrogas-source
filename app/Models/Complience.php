<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complience extends Model
{
    use HasFactory;
    
    protected $table = 'complience';

    protected $fillable = [
        'RegulasiID',
        'Pasal',
        'Ayat',
        'Complience',
        'StatusBasin',
        'StatusIsland',
        'KeyItems',
    ];

    protected $primaryKey = 'ID';

    public function RegulasiName(){
        return $this->belongsTo(Regulasi::class, 'RegulasiID', 'ID');
    }
}
