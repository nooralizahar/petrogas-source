<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TindakLanjut extends Model
{
    use HasFactory;
    
    protected $table = 'tindaklanjut';

    protected $fillable = [
        'ComplienceID',
        'TindakLanjut',
        'DueDate',
        'PIC',
        'Status'
    ];

    protected $primaryKey = 'ID';

    public function complienceName(){
        return $this->belongsTo(Complience::class, 'ComplienceID', 'ID');
    }
}
