<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Regulasi;
use App\Models\Complience;
use App\Models\TindakLanjut;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportRegulasi;
use App\Exports\ExportRegulasi;
use DataTables;
use DB;

class RegulasiController extends Controller
{
    function __construct() {
        
    }
    
    public function index(Request $request)
    {
        $data['all_tindak_lanjut'] = TindakLanjut::count();
        $data['all_regulasi'] = Regulasi::count();
        $data['all_complience'] = Complience::count();

        // Basin
        $data['not_complied_basin'] = Complience::where('StatusBasin', 'Not Complied')->count();
        $data['complied_basin'] = Complience::where('StatusBasin', 'Complied')->count();
        $data['not_applicable_basin'] = Complience::where('StatusBasin', 'Not Applicable')->count();

        if ($data['all_complience'] == 0 || $data['all_complience'] == NULL) {
            $data['not_complied_percentage_basin'] = 0;
            $data['complied_percentage_basin'] = 0;
            $data['not_applicable_percentage_basin'] = 0;
        } else {

            $data['not_complied_percentage_basin'] = ($data['not_complied_basin'] / $data['all_complience']) * 100;
            $data['complied_percentage_basin'] = ($data['complied_basin'] / $data['all_complience']) * 100;
            $data['not_applicable_percentage_basin'] = ($data['not_applicable_basin'] / $data['all_complience']) * 100;
        }

        // Island 
        $data['not_complied_island'] = Complience::where('StatusIsland', 'Not Complied')->count();
        $data['complied_island'] = Complience::where('StatusIsland', 'Complied')->count();
        $data['not_applicable_island'] = Complience::where('StatusIsland', 'Not Applicable')->count();

        if ($data['all_complience'] == 0 || $data['all_complience'] == NULL) {
            $data['not_complied_percentage_island'] = 0;
            $data['complied_percentage_island'] = 0;
            $data['not_applicable_percentage_island'] = 0;
        } else {

            $data['not_complied_percentage_island'] = ($data['not_complied_island'] / $data['all_complience']) * 100;
            $data['complied_percentage_island'] = ($data['complied_island'] / $data['all_complience']) * 100;
            $data['not_applicable_percentage_island'] = ($data['not_applicable_island'] / $data['all_complience']) * 100;
        }

        // Chart Tindakan Lanjut
        $labs = DB::raw("DAYNAME(DueDate) as label");
        $labels = TindakLanjut::select(
            $labs,
        )
        ->whereYear('DueDate', date('Y'))
        ->groupBy('label')
        ->orderBy('DueDate', 'ASC')
        ->get();

        $tindaklanjuts = TindakLanjut::select(
            $labs,
               DB::raw("(Count(TindakLanjut)) as tindaklanjut"),
            )
            ->whereYear('DueDate', date('Y'))
            ->groupBy('label')
            ->orderBy('DueDate', 'ASC')
            ->get();
            
        $dataLabel = [];
        $dataTindakLanjut = [];

        foreach ($labels as $row) {
            $dataLabel[] = $row['label'];
        }
        foreach ($tindaklanjuts as $row) {
            $dataTindakLanjut[] = (int)$row['tindaklanjut'];
        }
        $data['label'] = json_encode($dataLabel);

        $data['tindaklanjut'] = json_encode($dataTindakLanjut, JSON_NUMERIC_CHECK);

        return view('regulasi', $data);
    }
    
    public function ajax(Request $request)
    {
        $query = Regulasi::orderBy('ID', 'DESC'); 
        
        if ($request->searchByKelompok != NULL && $request->searchByBidang != NULL) {
            $query->where(function($row) use($request) {
                $row->where('Kelompok', $request->searchByKelompok )
                ->where ( 'Bidang', $request->searchByBidang );
            })->get();
        } elseif($request->searchByKelompok != NULL || $request->searchByBidang != NULL) {
            $query->where(function($row) use($request) {
                $row->where('Kelompok', $request->searchByKelompok )
                ->orWhere ( 'Bidang', $request->searchByBidang );
            })->get();
        } else {
            $query = $query->get();
        }
        
        return DataTables::of($query)->addIndexColumn()
        ->addColumn('Actions', function($data){
            return '
            <a href="/regulasi/detail/'.$data->ID.'" class="btn btn-sm btn-clean btn-icon btn-light mb-2" title="Detail">
            <i class="fas fa-file text-primary"></i>
            </a>
            <a href="/regulasi/form/'.$data->ID.'" class="btn btn-sm btn-clean btn-icon btn-light mb-2" title="Edit">
            <i class="far fa-edit text-primary"></i>
            </a>
            <a href="/regulasi/delete/'.$data->ID.'" class="btn btn-sm btn-clean btn-icon delete btn-light mb-2" title="Delete">
            <i class="fas fa-trash text-danger"></i>
            </a>
            <script src="/assets/alert.js"></script>';
        })
        ->editColumn('File', function($data){
            $file = '<a href="/regulasiFile/'.$data->File.'" target="_blank" class="text-primary">Baca File PDF</a>';
            if (isset($data->File)) {
                return $file;
            } else {
                return 'File Belum Diupload';
            }
        })
        ->rawColumns(['Actions', 'File'])
        ->make(true);
    }
    
    public function form($id = null)
    {
        $data['regulasiID'] = $id;
        $data['row'] = Regulasi::where('ID', $id)->first();
        return view('regulasi-form', $data);
    }
    
    public function save(Request $request)
    {
        if (isset($request->File)) {
            if ($request->File != null) {
                $file = $request->file('File');
                $fileName = date('YmdHi').'_'.$file->getClientOriginalName();
                $file->move(public_path('regulasiFile'), $fileName);
                
                $oldFile = $request->oldFile;
                $path=public_path().'/regulasiFile/'.$oldFile;
                
                if (isset($oldFile)) {
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }
                
            } else {
                $fileName = $request->oldFile;
            }
        } else {
            $fileName = $request->oldFile ?? NULL;
        }
        // dd(isset($request->Complience));

        if (!isset($request->ID)) {
            $update = Regulasi::create([
                'Peraturan' => $request->Peraturan,
                'No' => $request->No,
                'Tahun' => $request->Tahun,
                'Topik' => $request->Topik,
                'Kelompok' => $request->Kelompok,
                'Bidang' => $request->Bidang,
                'Regulasi' => $request->Regulasi,
                'CakupanRegulasi' => $request->CakupanRegulasi,
                'Konsekuensi' => $request->Konsekuensi,
                'File' => $fileName
            ]); 

            if (isset($request->kt_docs_repeater_basic)) {

                foreach ($request->kt_docs_repeater_basic as $key => $value) {
                    $store = Complience::create([
                        'RegulasiID' => $update->ID,
                        'Pasal' => $value['Pasal'],
                        'Ayat' => $value['Ayat'],
                        'Complience' => $value['Complience'],
                        'StatusBasin' => $value['StatusBasin'],
                        'StatusIsland' => $value['StatusIsland'],
                        'KeyItems' => $value['KeyItems'],
                    ]);
                }
            }
            return redirect()->route('regulasi')->with(['success' => 'Data berhasil di simpan']);
        } else {
        
            $update = Regulasi::where('id', $request->ID)->update([
                'Peraturan' => $request->Peraturan,
                'No' => $request->No,
                'Tahun' => $request->Tahun,
                'Topik' => $request->Topik,
                'Kelompok' => $request->Kelompok,
                'Bidang' => $request->Bidang,
                'Regulasi' => $request->Regulasi,
                'CakupanRegulasi' => $request->CakupanRegulasi,
                'Konsekuensi' => $request->Konsekuensi,
                'File' => $fileName
            ]); 
            return redirect()->route('regulasi')->with(['success' => 'Data berhasil di update']);
        }
    }
    
    public function delete($id = null)
    {
        $data = Regulasi::find($id);
        $get_regulasi = Regulasi::where('ID', $id)->first();
        $get_complience = Complience::where('regulasiID', $data->ID)->get();
        foreach ($get_complience as $key) {
            $get_tindak_lanjut = TindakLanjut::where('ComplienceID', $key->ID)->get();
            foreach ($get_tindak_lanjut as $value) {
                $delete_tindak_lanjut = TindakLanjut::find($value->ID);
                $delete_tindak_lanjut->delete();
            }
            $delete_complience = Complience::find($key->ID);
            $delete_complience->delete();
        }
        if ($get_regulasi->File != NULL || $get_regulasi->File != "") {
            $path=public_path().'/regulasiFile/'.$get_regulasi->File;
            
            if (isset($get_regulasi->File)) {
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        }
        $data->delete();
        
        return redirect()->route('regulasi')->with(['success' => 'Data berhasil di hapus']);
    }
    
    public function detail($id = null)
    {
        $data['regulasiID'] = $id;

        $data['row'] = Regulasi::where('ID', $id)->first();
        $data['complience'] = Complience::groupBy('Pasal', 'RegulasiID')->where('RegulasiID', $id)->get();
        $data['all_complience'] = Complience::where('RegulasiID', $id)->count();

        // Basin
        $data['not_complied_basin'] = Complience::where('StatusBasin', 'Not Complied')->where('RegulasiID', $id)->count();
        $data['complied_basin'] = Complience::where('StatusBasin', 'Complied')->where('RegulasiID', $id)->count();
        $data['not_applicable_basin'] = Complience::where('StatusBasin', 'Not Applicable')->where('RegulasiID', $id)->count();
        if ($data['all_complience'] == 0 || $data['all_complience'] == NULL) {
            $data['percentage_basin'] = 0;
            $data['complied_percentage_basin'] = 0;
        } else {
            $data['percentage_basin'] = ($data['complied_basin'] / $data['all_complience']) * 100;
            $data['complied_percentage_basin'] = ($data['complied_basin'] / $data['all_complience']) * 100;
        }

        // Island
        $data['not_complied_island'] = Complience::where('StatusIsland', 'Not Complied')->where('RegulasiID', $id)->count();
        $data['complied_island'] = Complience::where('StatusIsland', 'Complied')->where('RegulasiID', $id)->count();
        $data['not_applicable_island'] = Complience::where('StatusIsland', 'Not Applicable')->where('RegulasiID', $id)->count();
        if ($data['all_complience'] == 0 || $data['all_complience'] == NULL) {
            $data['percentage_island'] = 0;
            $data['complied_percentage_island'] = 0;
        } else {
            $data['percentage_island'] = ($data['complied_island'] / $data['all_complience']) * 100;
            $data['complied_percentage_island'] = ($data['complied_island'] / $data['all_complience']) * 100;
        }

        return view('regulasi-detail', $data);
    }

    public function export($id = null){
        return Excel::download(new ExportRegulasi, 'regulasi.xlsx');
    }

    public function import(Request $request){
        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('import',$nama_file);
		Excel::import(new ImportRegulasi, public_path('/import/'.$nama_file));
        
		return redirect()->route('regulasi')->with(['success' => 'Data berhasil di import']);
    }
}
