<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Regulasi;
use App\Models\Complience;
use App\Models\TindakLanjut;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportTindakanLanjut;
use App\Exports\ExportTindakanLanjut;

class TindakLanjutController extends Controller
{
    function __construct() {
        
    }

    public function index(Request $request)
    {
        $data['complience'] = Complience::groupBy('Pasal', 'RegulasiID')->get();
        return view('tindak-lanjut', $data);
    }

    public function ajax(Request $request)
    {
        if (isset($request->searchByRegulasiID)) {
            $get_complience = Complience::where('RegulasiID', $request->searchByRegulasiID)->get(); 
            $complienceID = [];
            foreach ($get_complience as $key) {
                array_push($complienceID, $key->ID);
            }
            $query = TindakLanjut::orderBy('ID', 'DESC')->whereIn('ComplienceID', $complienceID)->get();    
        } else {
            $query = TindakLanjut::orderBy('ID', 'DESC')->get();    
        }

        if (isset($request->searchByStatus)) {
            $query = $query->where('Status', $request->searchByStatus);
        }
        
        return DataTables::of($query)->addIndexColumn()
        ->addColumn('Actions', function($data){
            return '
            <a data-id="'.$data->ID.'" data-pasal="'.$data->complienceName->Pasal.','.$data->complienceName->RegulasiID.'" data-ayat="'.$data->complienceName->Ayat.'" data-tindaklanjut="'.$data->TindakLanjut.'" data-duedate="'.$data->DueDate.'" data-pic="'.$data->PIC.'" data-status="'.$data->Status.'" class="btn btn-sm btn-clean btn-icon btn-light update mb-2" title="Edit">
            <i class="far fa-edit text-primary"></i>
            </a>
            <a href="/tindak-lanjut/delete/'.$data->ID.'" class="btn btn-sm btn-clean btn-icon delete btn-light mb-2" title="Delete">
            <i class="fas fa-trash text-danger"></i>
            </a>
            <script src="/assets/alert.js"></script>
            <script>
            $(document).on("click", ".update", function() {
                var id = $(this).data("id");
                var pasal = $(this).data("pasal");
                var ayat = $(this).data("ayat");
                var tindaklanjut = $(this).data("tindaklanjut");
                var duedate = $(this).data("duedate");
                var pic = $(this).data("pic");
                var status = $(this).data("status");
                $(".ID").val(id);
                $(".Pasal").val(pasal).trigger("change");
                $(".AyatVal").val(ayat);
                $(".TindakLanjut").val(tindaklanjut);
                $(".DueDate").val(duedate);
                $(".PIC").val(pic);
                $(".Status").val(status).trigger("change");
                $(".modalUpdate").modal("show")
            });
            </script>';
        })
        ->addColumn('Pasal', function($data){
            return $data->complienceName->Pasal ?? '';
        })
        ->addColumn('Ayat', function($data){
            return $data->complienceName->Ayat ?? '';
        })
        ->editColumn('ComplienceID', function($data){
            return $data->complienceName->Complience ?? '';
        })
        ->editColumn('Status', function($data){
            return $data->Status;
        })
        ->editColumn('DueDate', function($data){
            return '<b>'.date('d/m/Y', strtotime($data->DueDate)).'</b><br>'.$data->PIC ?? '-';
        })
        ->rawColumns(['Actions', 'DueDate', 'Status'])
        ->make(true);
    }

    public function save(Request $request)
    {
        $explode = explode(",", $request->Pasal);
        $get_complince_id = Complience::where('Pasal', $explode[0])->where('Ayat', $request->Ayat)->first();
        if (isset($get_complince_id)) {
            $get_complince_id_count = TindakLanjut::where('ComplienceID', $get_complince_id->ID)->count();
            if ($get_complince_id_count >= 1) {
                return redirect()->back()->with(['error' => 'Tindakan Lanjut untuk Compliance berikut sudah ada']);
            }
            $update = TindakLanjut::create([
                'ComplienceID' => $get_complince_id->ID,
                'TindakLanjut' => $request->TindakLanjut,
                'DueDate' => $request->DueDate,
                'PIC' => $request->PIC,
                'Status' => $request->Status,
            ]); 
            return redirect()->back()->with(['success' => 'Data berhasil di simpan']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal di simpan']);
        }
    }

    public function update(Request $request)
    {
        $explode = explode(",", $request->Pasal);
        $get_complince_id = Complience::where('Pasal', $explode[0])->where('Ayat', $request->Ayat)->first();
        if (isset($get_complince_id)) {
            $get_complince_id_count = TindakLanjut::where('ComplienceID', $get_complince_id->ID)->where('ID', '!=', $request->ID)->count();
            if ($get_complince_id_count >= 1) {
                return redirect()->back()->with(['error' => 'Tindakan Lanjut untuk Compliance berikut sudah ada']);
            }
            $update = TindakLanjut::where('id', $request->ID)->update([
                'TindakLanjut' => $request->TindakLanjut,
                'DueDate' => $request->DueDate,
                'PIC' => $request->PIC,
                'Status' => $request->Status,
            ]); 
            return redirect()->back()->with(['success' => 'Data berhasil di update']);
        } else {
            return redirect()->back()->with(['error' => 'Data gagal di update']);
        }
    }
    
    public function delete($id = null)
    {
        $data = TindakLanjut::find($id);
        $data->delete();

        // dd($id);
        
        return redirect()->back()->with(['success' => 'Data berhasil di hapus']);
    }

    public function list_ayat($array = null)
    {
        $explode = explode(",", $array);
        $data = Complience::where('RegulasiID', $explode[1])->where('Pasal', $explode[0])->pluck('ID', 'Ayat');
        return response()->json($data);
    }

    public function export($RegulasiID = null){
        return Excel::download(new ExportTindakanLanjut($RegulasiID), 'tindakan_lanjut.xlsx');
    }

    public function import(Request $request){
        $this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		$file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();
		$file->move('import',$nama_file);
		Excel::import(new ImportTindakanLanjut($request->RegulasiID), public_path('/import/'.$nama_file));
        
		return redirect()->back()->with(['success' => 'Data berhasil di import']);
    }
}
