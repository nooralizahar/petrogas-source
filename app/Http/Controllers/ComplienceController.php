<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Regulasi;
use App\Models\Complience;
use App\Models\TindakLanjut;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportCompliance;
use App\Exports\ExportCompliance;

class ComplienceController extends Controller
{
    function __construct() {
        
    }
    
    public function index(Request $request)
    {
        return view('complience');
    }
    
    public function ajax(Request $request)
    {
        
        if (isset($request->searchByRegulasiID)) {
            $query = Complience::orderBy('ID', 'DESC')->where('RegulasiID', $request->searchByRegulasiID);    
        } else {
            $query = Complience::orderBy('ID', 'DESC');    
        }
        
        if ($request->searchByStatusBasin != NULL && $request->searchByStatusIsland != NULL) {
            $query->where(function($row) use($request) {
                $row->where('StatusBasin', $request->searchByStatusBasin )
                ->where('StatusIsland', $request->searchByStatusIsland );
            })->get();
        } elseif($request->searchByStatusBasin != NULL || $request->searchByStatusIsland != NULL) {
            $query->where(function($row) use($request) {
                $row->where('StatusBasin', $request->searchByStatusBasin )
                ->orWhere('StatusIsland', $request->searchByStatusIsland );
            })->get();
        } else {
            $query = $query->get();
        }
        
        return DataTables::of($query)->addIndexColumn()
        ->addColumn('Actions', function($data){
            return "
            <a data-id='".$data->ID."' data-pasal='".$data->Pasal."' data-ayat='".$data->Ayat."' data-complience='".$data->Complience."' data-keyitems='".$data->KeyItems."' data-statusbasin='".$data->StatusBasin."' data-statusisland='".$data->StatusIsland."' class='btn btn-sm btn-clean btn-icon btn-light update_complience mb-2' title='Edit'>
            <i class='far fa-edit text-primary'></i>
            </a>
            <a href='/complience/delete/".$data->ID."' class='btn btn-sm btn-clean btn-icon delete btn-light mb-2' title='Delete'>
            <i class='fas fa-trash text-danger'></i>
            </a>
            <script src='/assets/alert.js'></script>
            <script>
            $(document).on('click', '.update_complience', function() {
                var id = $(this).data('id');
                var pasal = $(this).data('pasal');
                var ayat = $(this).data('ayat');
                var complience = $(this).data('complience');
                var keyitems = $(this).data('keyitems');
                var statusbasin = $(this).data('statusbasin');
                var statusisland = $(this).data('statusisland');
                $('.ID').val(id);
                $('.Pasal').val(pasal);
                $('.Ayat').val(ayat);
                $('.Complience').val(complience);
                $('.KeyItems').summernote('code', $(this).data('keyitems'));
                $('.StatusBasin').val(statusbasin).trigger('change');
                $('.StatusIsland').val(statusisland).trigger('change');
                $('.modalUpdateComplience').modal('show')
            });
            </script>
            ";
        })
        ->editColumn('KeyItems', function($data){
            if (isset($data->KeyItems)) {
                return "<a type='button' data-keyitems='".$data->KeyItems."' class='text-primary font-weight-bold key_items'><u>Baca Key Items</u></a>
                <script>
                $(document).on('click', '.key_items', function() {
                    $('.KeyItems').html($(this).data('keyitems'));
                    $('.modalKeyItems').modal('show')
                });
                </script>";
            } else {
                return "<a type='button' class='text-muted font-weight-bold' disabled><u>Baca Key Items</u></a>";
            }
        })
        ->editColumn('StatusBasin', function($data){
            return $data->StatusBasin;
        })
        ->editColumn('StatusIsland', function($data){
            return $data->StatusIsland;
        })
        ->rawColumns(['Actions', 'KeyItems'])
        ->make(true);
    }
    
    public function save(Request $request)
    {
        $get_complince_id_count = Complience::where(function($row) use($request) {
            $row->where('ID', '!=', $request->ID )
            ->where( 'Pasal', $request->Pasal )
            ->where( 'Ayat', $request->Ayat );
        })->count();
        if ($get_complince_id_count >= 1) {
            return redirect()->back()->with(['error' => 'Complience dengan Ayat atau Pasal berikut sudah ada']);
        }
        $update = Complience::create([
            'RegulasiID' => $request->RegulasiID,
            'Pasal' => $request->Pasal,
            'Ayat' => $request->Ayat,
            'Complience' => $request->Complience,
            'KeyItems' => $request->KeyItems,
            'StatusBasin' => $request->StatusBasin,
            'StatusIsland' => $request->StatusIsland,
        ]); 
        return redirect()->back()->with(['success' => 'Data berhasil di simpan']);
    }
    
    public function update(Request $request)
    {
        $get_complince_id_count = Complience::where(function($row) use($request) {
            $row->where('ID', '!=', $request->ID )
            ->where( 'RegulasiID', $request->RegulasiID )
            ->where( 'Pasal', $request->Pasal )
            ->where( 'Ayat', $request->Ayat );
        })->count();
        if ($get_complince_id_count >= 1) {
            return redirect()->back()->with(['error' => 'Complience dengan Ayat atau Pasal berikut sudah ada']);
        }
        $update = Complience::where('id', $request->ID)->update([
            'Pasal' => $request->Pasal,
            'Ayat' => $request->Ayat,
            'Complience' => $request->Complience,
            'KeyItems' => $request->KeyItems,
            'StatusBasin' => $request->StatusBasin,
            'StatusIsland' => $request->StatusIsland,
        ]); 
        return redirect()->back()->with(['success' => 'Data berhasil di update']);
    }
    
    public function delete($id = null)
    {
        $data = Complience::find($id);
        $get_tindak_lanjut = TindakLanjut::where('ComplienceID', $data->ID)->get();
        foreach ($get_tindak_lanjut as $value) {
            $delete_tindak_lanjut = TindakLanjut::find($value->ID);
            $delete_tindak_lanjut->delete();
        }
        $data->delete();
        
        return redirect()->back()->with(['success' => 'Data berhasil di hapus']);
    }
    
    public function export($RegulasiID = null){
        return Excel::download(new ExportCompliance($RegulasiID), 'compliance.xlsx');
    }
    
    public function import(Request $request){
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        
        $file = $request->file('file');
        $nama_file = rand().$file->getClientOriginalName();
        $file->move('import',$nama_file);
        Excel::import(new ImportCompliance($request->RegulasiID), public_path('/import/'.$nama_file));
        
        return redirect()->back()->with(['success' => 'Data berhasil di import']);
    }
}
