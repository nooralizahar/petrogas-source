<?php

namespace App\Exports;

use App\Models\Regulasi;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportRegulasi implements FromQuery, WithHeadings, WithStyles, ShouldAutoSize
{
    use Exportable;

    public function __construct()
    {
        // $this->id = $id;
    }

    public function query()
    {
        return Regulasi::select('Peraturan','No', 'Tahun', 'Topik', 'Kelompok', 'Bidang', 'Regulasi');
    }

    public function headings(): array
    {
        return ["Peraturan", "No", "Tahun", "Topik", "Kelompok", "Bidang", "Regulasi"];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
