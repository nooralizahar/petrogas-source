<?php

namespace App\Exports;

use App\Models\TindakLanjut;
use App\Models\Complience;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportTindakanLanjut implements FromQuery, WithHeadings, WithStyles, ShouldAutoSize
{
    use Exportable;
    
    public function  __construct($RegulasiID)
    {
        $this->RegulasiID= $RegulasiID;
    }

    public function query()
    {
        if (isset($this->RegulasiID)) {
            $get_complience = Complience::where('RegulasiID', $this->RegulasiID)->get(); 
            $complienceID = [];
            foreach ($get_complience as $key) {
                array_push($complienceID, $key->ID);
            }

            return TindakLanjut::select('complience.Pasal','complience.Ayat', 'complience.Complience', 'TindakLanjut', 'DueDate', 'PIC', 'Status')
            ->leftJoin('complience', 'tindaklanjut.ComplienceID', '=', 'complience.ID')->whereIn('ComplienceID', $complienceID);
        } else {
            return TindakLanjut::select('complience.Pasal','complience.Ayat', 'complience.Complience', 'TindakLanjut', 'DueDate', 'PIC', 'Status')
            ->leftJoin('complience', 'tindaklanjut.ComplienceID', '=', 'complience.ID');
        }
    }

    public function headings(): array
    {
        return ['Pasal','Ayat', 'Complience', 'TindakanLanjut', 'DueDate', 'PIC', 'Status'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
