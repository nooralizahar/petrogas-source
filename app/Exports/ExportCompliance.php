<?php

namespace App\Exports;

use App\Models\Complience;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportCompliance implements FromQuery, WithHeadings, WithStyles, ShouldAutoSize
{
    use Exportable;

    public function  __construct($RegulasiID)
    {
        $this->RegulasiID= $RegulasiID;
    }
    
    public function query()
    {
        if (isset($this->RegulasiID)) {
            return Complience::select('Pasal','Ayat', 'Complience', 'StatusBasin', 'StatusIsland', 'KeyItems')->where('RegulasiID', $this->RegulasiID);
        } else {
            return Complience::select('Pasal','Ayat', 'Complience', 'StatusBasin', 'StatusIsland', 'KeyItems');
        }
    }
    
    public function headings(): array
    {
        return ['Pasal','Ayat', 'Complience', 'StatusBasin', 'StatusIsland', 'KeyItems'];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],
        ];
    }
}
