<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Regulasi;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportRegulasi implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Regulasi([
            'Peraturan' => $row[0] ?? '',
            'No' => $row[1] ?? '', 
            'Tahun' => $row[2] ?? '',
            'Topik' => $row[3] ?? '',
            'Kelompok' => $row[4] ?? '',
            'Bidang' => $row[5] ?? '',
            'Regulasi' => $row[6] ?? '',
            'CakupanRegulasi' => $row[7] ?? '',
            'Konsekuensi' => $row[8] ?? ''
        ]);
    }
    
    public function startRow(): int
    {
        return 2;
    }
}
