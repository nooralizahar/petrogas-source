<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Complience;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportCompliance implements ToCollection, WithStartRow
{
    /**
    * @param Collection $collection
    */
    public function  __construct($RegulasiID)
    {
        $this->RegulasiID= $RegulasiID;
    }
    
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            Complience::create([
                'RegulasiID' => $this->RegulasiID ?? '',
                'Pasal' => $row[0] ?? '',
                'Ayat' => $row[1] ?? '', 
                'Complience' => $row[2] ?? '',
                'StatusBasin' => $row[3] ?? '',
                'StatusIsland' => $row[4] ?? '',
                'KeyItems' => $row[5] ?? ''
            ]);
        }
    }
    
    public function startRow(): int
    {
        return 2;
    }
}
