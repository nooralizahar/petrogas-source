<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\TindakLanjut;
use App\Models\Complience;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ImportTindakanLanjut implements ToCollection, WithStartRow
{
    /**
    * @param Collection $collection
    */

    public function  __construct($RegulasiID)
    {
        $this->RegulasiID= $RegulasiID;
    }
    
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $get_complience = Complience::where('RegulasiID', $this->RegulasiID)->where('Pasal',  $row[0])->where('Ayat', $row[1])->first();
            if (isset($get_complience)) {
                TindakLanjut::create([
                    'ComplienceID' => $get_complience->ID ?? '',
                    'TindakLanjut' => $row[2] ?? '',
                    'DueDate' => $row[3] ?? '',
                    'PIC' => $row[4] ?? '',
                    'Status' => $row[5] ?? ''
                ]);
            }
        }
    }
    
    public function startRow(): int
    {
        return 2;
    }
}
